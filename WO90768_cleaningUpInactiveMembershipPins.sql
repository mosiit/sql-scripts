-- Run on Wolverine/TEST 7/25/18
-- Run on Superman/LIVE 8/8/18

SELECT COUNT(*)
FROM dbo.TX_CUST_MEMBERSHIP 
WHERE DATEDIFF(d,GETDATE(),expr_dt) < 0  -- expired membereships
-- T - 1,126,474
-- L - 1,131,489

SELECT * FROM dbo.LT_MOS_MEMBER_CARDS WHERE customer_no = 0 
-- 0 Good! No records where customer_no = 0

SELECT cust_memb_no, COUNT(*) FROM dbo.TX_CUST_MEMBERSHIP GROUP BY cust_memb_no HAVING COUNT(*) > 1
-- 0 Good! cust_memb_no not duplicated in tx_cust_membership

-- LIFETIME MEMBERSHIPS
SELECT m.cust_memb_no, c.customer_no, c.display_name, m.memb_level, m.memb_amt, m.init_dt, m.expr_dt, m.current_status, m.cur_record, m.create_dt, m.notes
FROM dbo.TX_CUST_MEMBERSHIP m
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS c
			ON m.customer_no = c.customer_no
WHERE YEAR(expr_dt) > 2020
ORDER BY expr_dt 
-- T - 152
-- L - 154

-- QUERIES 
-- 0. DELETE DUPLICATE ROWS
-- 1. WHEN CUST_MEMB_NO = 0, THESE WERE CREATED IN MAY 2016. ALL THESE PINS CAN BE SET TO INACTIVE, EXCEPT THOSE THAT ARE HAVE LIFETIME MEMBERSHIPS.  
-- 2. WHEN CUST_MEMB_NO AND CUSTOMER_NO MATCH IN TX_CUST_MEMBERSHIP AND LT_MOS_MEMBER_CARDS. IF EXPIRED MEMBERSHIP, SET PIN INACTIVE = 'Y'
-- 3. WHEN CUST_MEMB_NO MATCH IN TX_CUST_MEMBERSHIP AND CUSTOMER_NO DOES NOT MATCH
-- 4. MEMBERSHIPS HAVING MORE THAN ONE PIN
-- 5. CUSTOMER_NO HAVING MORE THAN 1 ACTIVE PIN

-- 0. DELETE DUPLICATE ROWS
;
WITH DupData
AS
(	-- number the rows so that we only delete the extras and keep those with rownum = 1
	SELECT ROW_NUMBER() OVER(PARTITION BY mcrds.customer_no, mcrds.cust_memb_no, mcrds.pin, mcrds.printed_dt, mcrds.sli_no, mcrds.inactive,
							mcrds.created_by, mcrds.create_dt, mcrds.create_loc --, mcrds.last_updated_by, mcrds.last_update_dt
							ORDER BY id ) AS rownum,
		mcrds.id, mcrds.customer_no, mcrds.cust_memb_no, mcrds.pin, mcrds.printed_dt, mcrds.sli_no, mcrds.inactive, 
		mcrds.created_by, mcrds.create_dt, mcrds.create_loc --, mcrds.last_updated_by, mcrds.last_update_dt
	FROM dbo.LT_MOS_MEMBER_CARDS mcrds
	INNER JOIN 
	(	-- get all records that are exact duplicates in the LT_MOS_MEMEBER_CARDS table. 
		SELECT customer_no, cust_memb_no, pin, printed_dt, sli_no, inactive,created_by,create_dt, create_loc, /*last_updated_by, last_update_dt,*/ COUNT(*) AS cnt
		FROM dbo.LT_MOS_MEMBER_CARDS
		GROUP BY customer_no, cust_memb_no, pin, printed_dt, sli_no, inactive,created_by,create_dt, create_loc --, last_updated_by, last_update_dt
		HAVING COUNT(*) > 1 
		-- T - 1825
		-- L - 1832
	) dups
	ON dups.customer_no = mcrds.customer_no
	AND dups.cust_memb_no = mcrds.cust_memb_no
	AND dups.pin = mcrds.pin
	AND dups.printed_dt = mcrds.printed_dt
	AND ISNULL(dups.sli_no, '0') = ISNULL(mcrds.sli_no,'0')
	AND dups.inactive = mcrds.inactive
	AND dups.created_by = mcrds.created_by
	AND dups.create_dt = mcrds.create_dt
	AND dups.create_loc = mcrds.create_loc
	--AND ISNULL(dups.last_updated_by,'x') = ISNULL(mcrds.last_updated_by,'x')
	--AND ISNULL(dups.last_update_dt,'1900-01-01') = ISNULL(mcrds.last_update_dt,'1900-01-01')
-- T - 4507
-- L - 4522 
) 
SELECT * 
--DELETE m
FROM dbo.LT_MOS_MEMBER_CARDS m
INNER JOIN DupData d
	ON m.id = d.id
WHERE d.rownum <> 1
-- T - 2682
-- L - 2690

-- 1. WHEN CUST_MEMB_NO = 0, THESE WERE CREATED IN MAY 2016. ALL THESE PINS CAN BE SET TO INACTIVE, EXCEPT THOSE THAT ARE HAVE LIFETIME MEMBERSHIPS.  

-- FILTER OUT LIFETIME MEMBERSHIPS
SELECT * 
FROM LT_MOS_MEMBER_CARDS 
WHERE cust_memb_no = 0 AND inactive = 'N' 
	AND customer_no NOT IN (
	SELECT c.customer_no -- , c.display_name, m.memb_level, m.memb_amt, m.expr_dt, m.current_status, m.cur_record, m.create_dt, m.notes
	FROM dbo.TX_CUST_MEMBERSHIP m
	INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS c
				ON m.customer_no = c.customer_no
	WHERE YEAR(expr_dt) > 2020
	-- T - 152
	-- L - 154
	)
-- 230137, only 2 cards are marked as Inactive = 'Y' from May 2016 Go Live
-- max(create_dt) -- 2016-05-17 19:24:38.983
-- T, L - 230,135

--UPDATE LT_MOS_MEMBER_CARDS 
--SET inactive = 'Y'
--WHERE cust_memb_no = 0 AND inactive = 'N'
--AND customer_no NOT IN (
--	SELECT c.customer_no -- , c.display_name, m.memb_level, m.memb_amt, m.expr_dt, m.current_status, m.cur_record, m.create_dt, m.notes
--	FROM dbo.TX_CUST_MEMBERSHIP m
--	INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS c
--				ON m.customer_no = c.customer_no
--	WHERE YEAR(expr_dt) > 2020 
--)
-- T, L 230,135

-- 2. WHEN CUST_MEMB_NO AND CUSTOMER_NO MATCH IN TX_CUST_MEMBERSHIP AND LT_MOS_MEMBER_CARDS. IF EXPIRED MEMBERSHIP, SET PIN INACTIVE = 'Y'

SELECT mem.cust_memb_no, mem.customer_no, mem.expr_dt, mem.current_status, mem.cur_record, crd.pin, crd.printed_dt, crd.inactive, crd.created_by
FROM dbo.TX_CUST_MEMBERSHIP mem
INNER JOIN dbo.LT_MOS_MEMBER_CARDS crd
	ON crd.cust_memb_no = mem.cust_memb_no AND crd.customer_no = mem.customer_no
WHERE crd.cust_memb_no <> 0 
	AND DATEDIFF(d,GETDATE(),expr_dt) < 0  -- expired membereships
	AND crd.inactive = 'N'
-- T - 68,684
-- L - 73,630

--UPDATE crd
--SET inactive = 'Y'
--FROM dbo.TX_CUST_MEMBERSHIP mem
--INNER JOIN dbo.LT_MOS_MEMBER_CARDS crd
--	ON crd.cust_memb_no = mem.cust_memb_no AND crd.customer_no = mem.customer_no
--WHERE crd.cust_memb_no <> 0 
--	AND DATEDIFF(d,GETDATE(),expr_dt) < 0  -- expired membereships
--	AND crd.inactive = 'N'
-- T - 68,684
-- L - 73,630

-- Also delete all expired cards based on cust_memb_no, even though the customer_no differs. There is no reason to worry about these. 
SELECT * 
--UPDATE crd
--SET crd.inactive = 'Y'
FROM dbo.TX_CUST_MEMBERSHIP mem
INNER JOIN dbo.LT_MOS_MEMBER_CARDS crd
	ON crd.cust_memb_no = mem.cust_memb_no
WHERE crd.cust_memb_no <> 0 
	AND DATEDIFF(d,GETDATE(),expr_dt) < 0  -- expired membereships
	AND crd.inactive = 'N'
-- T, L - 111 

-- 3. WHEN CUST_MEMB_NO MATCH IN TX_CUST_MEMBERSHIP AND CUSTOMER_NO DOES NOT MATCH
-- THESE WILL BE RECORDS FOR ANNIE/SUE/ANGELA P TO REVIEW. 
SELECT mem.cust_memb_no membership_cust_memb_no, mem.customer_no AS membership_customer_no, 
	mem.memb_level, mem.init_dt, mem.expr_dt, mem.current_status, mem.cur_record,
	crd.customer_no lt_mos_memb_card_customer_no, crd.pin, crd.printed_dt, 
	crd.inactive AS lt_mos_memb_card_inactive, crd.created_by AS lt_mos_memb_card_created_by, crd.create_dt AS lt_mos_memb_card_create_Dt
FROM dbo.TX_CUST_MEMBERSHIP mem
INNER JOIN dbo.LT_MOS_MEMBER_CARDS crd
	ON crd.cust_memb_no = mem.cust_memb_no 
WHERE crd.customer_no <> mem.customer_no
	AND crd.cust_memb_no <> 0 
	AND crd.inactive = 'N'
ORDER BY crd.customer_no, mem.expr_dt desc
-- T - 119 inactive = 'N'
-- L - 7 

-- 4. MEMBERSHIPS HAVING MORE THAN ONE ACTIVE PIN
-- THESE WILL BE RECORDS FOR ANNIE/SUE/ANGELA P TO REVIEW. 
SELECT cust_memb_no, customer_no, COUNT(*) cnt
FROM dbo.LT_MOS_MEMBER_CARDS 
WHERE cust_memb_no <> 0 AND inactive = 'N'
GROUP BY cust_memb_no, customer_no
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 
-- 2 after 3 above cleanups run. 

-- 5. CUSTOMER_NO HAVING MORE THAN 1 ACTIVE PIN 
-- THIS IS THE ISSUE WE ARE TRYING TO FIX.
SELECT customer_no, COUNT(*) cnt
FROM LT_MOS_MEMBER_CARDS 
WHERE cust_memb_no <> 0 AND inactive = 'N'
GROUP BY customer_no
HAVING COUNT(*) > 2
ORDER BY COUNT(*) DESC 
-- 24 after 3 above cleanups run. 

	--SELECT * FROM dbo.LT_MOS_MEMBER_CARDS WHERE customer_no = 12831 

-- 6. HOW MANY EXPIRED MEMBERSHIPS ARE MARKED AS STILL ACTIVE??? 
SELECT mem.cust_memb_no, mem.customer_no, mem.expr_dt, mem.current_status, mem.cur_record, crd.pin, crd.printed_dt, crd.inactive, crd.created_by
FROM dbo.TX_CUST_MEMBERSHIP mem
INNER JOIN dbo.LT_MOS_MEMBER_CARDS crd
	ON crd.cust_memb_no = mem.cust_memb_no
WHERE crd.cust_memb_no <> 0 
	AND DATEDIFF(d,GETDATE(),expr_dt) < 0  -- expired membereships
	AND crd.inactive = 'N'
-- T - 111 rows 
-- L - 0 

