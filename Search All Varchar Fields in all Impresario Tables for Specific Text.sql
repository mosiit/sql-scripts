
    IF OBJECT_ID('tempdb..#results') is not null DROP TABLE [#results];

    CREATE TABLE [#results] ([schema_name] VARCHAR(100), 
                             [table_name] varchar(100), 
                             [column_name] VARCHAR(100), 
                             [search_str] VARCHAR(255), 
                             [found] INT)

    DECLARE @srchStr VARCHAR(255) = 'Beast'
    DECLARE @sqlCommand VARCHAR(2000)

    DECLARE BigSearchCursor INSENSITIVE CURSOR FOR
    SELECT 'INSERT INTO [#results] ([schema_name], [table_name], [column_name], [search_str], [found])' + CHAR(10)
         + '    SELECT ''' + s.[name] + ''', ''' + o.[name] + ''', ''' + c.[name] + ''', ''' + @srchStr + ''', count(*)' + CHAR(10)
         + '    FROM [' + s.[name] + '].[' + o.[name] + ']' + CHAR(10)
         + '    WHERE [' + c.[name] + '] like ''%' + @srchStr + '%''' + CHAR(10) + CHAR(10)
    FROM sys.columns AS c
         INNER JOIN sys.[objects] AS o ON o.[object_id] = c.[object_id]
         INNER JOIN sys.[types] AS t ON t.[system_type_id] = c.[system_type_id]
         INNER JOIN sys.[schemas] AS s ON s.[schema_id] = o.[schema_id]
    WHERE o.[type] = 'U' 
      AND t.[name] LIKE '%varchar%'
      AND LEFT(o.[name],2) <> 'C_'
      AND LEFT(o.[name],3) NOT IN ('CT_','CW_','CX_','NC_')
      AND LEFT(o.[name],4) NOT in ('bak_','ccc_')
      AND LEFT(o.[name],5) NOT IN ('bak2_','gooes','zDEL_')
      AND o.[name] NOT LIKE '%AUDIT%'
      AND o.[name] NOT IN ('aa','AspNet_SqlCacheTablesForChangeNotification','changelog','dtproperties','EIS_TEMP_CAPACITY','jb_order_entitlements','pbcatcol')
      AND c.[name] NOT IN ('created_by','create_loc','last_updated_by')
    ORDER BY o.[name], c.[column_id]
    OPEN [BigSearchCursor]
    BEGIN_BIG_SEARCH_LOOP:

        FETCH NEXT FROM [BigSearchCursor] INTO @sqlCommand
        IF @@FETCH_STATUS = -1 GOTO END_BIG_SEARCH_LOOP

        --COMMENT THE EXECUTE COMMAND AND UNCOMMENT THE PRINT COMMAND TO SEE THE SQL CODE THAT IS GOING TO BE EXECUTED
        PRINT (@sqlCommand)

        --COMMENT THE PRINT COMMAND AND UNCOMMENT THE EXECUTE COMMAND TO ACTUALLY RUN THE SQL CODE AND POPULATE THE TEMP TABLE
        --EXECUTE (@sqlCommand)

        GOTO BEGIN_BIG_SEARCH_LOOP

    END_BIG_SEARCH_LOOP:
    CLOSE [BigSearchCursor]
    DEALLOCATE [BigSearchCursor]

    DELETE FROM [#results] WHERE [found] = 0

    SELECT * FROM [#results]
