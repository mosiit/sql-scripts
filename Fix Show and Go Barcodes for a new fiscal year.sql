/*
    This script fixes the bar code mapping in the T_EXTERNAL_BARCODE_DATA table when a fiscal year turns over.
    Enter the old and new fiscal years into the variables below and run.  ***NOTE: Script can take up to two minutes plus to run.
                                                                                   Most recent run took 2:40

    NOTES:  The performance date on the new Show and go performance for the new fiscal year must be July 1 for this to work
            This should be run after closing on June 30th or sometime early on July 1.  
            If run on July 2nd or later, it will not work unless you change one line in the code... 

                SELECT @new_perf_date = CONVERT(CHAR(4),@old_fiscal_year) + '/07/01'
                
            The /07/01 (July 1) needs to be changed to whatever the current date is on the show and go performance

            For this to work, there can only be one valid show and go order for each show and go constituent.  Two orders are okay if one has been returned.
            If one constituent has more than one valid show and go orders in the same fiscal year, there will be issues.  
            That should never be the case but if an error pops up, that's something to check.
*/

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    --RUN THE FIRST STATEMENT (CHANGE THE DATE AT THE END OF THE TABLE NAME) TO SAVE A COPY OF THE CURRENT DATE IN THE TEMPORARYDATA DATABASE
    --SELECT * INTO [TemporaryData].[dbo].[T_EXTERNAL_BARCODE_DATA_20210630] FROM [impresario].[dbo].[T_EXTERNAL_BARCODE_DATA]
    --SELECT * FROM [TemporaryData].[dbo].[T_EXTERNAL_BARCODE_DATA_20210630]

    DECLARE @old_fiscal_year INT = 2021     --SET TO FISCAL YEAR THAT IS ENDING
    DECLARE @new_fiscal_year INT = 2022     --SET TO FISCAL YEAR THAT IS BEGINNING (THIS VARIABLE NO LONGER TECHICALLY BEING USED BUT LEFT IN TO SERVE AS INFORMATIONAL)
  
    DECLARE @change_data VARCHAR(3) = 'No'  --SET TO NO TO JUST SEE WHAT WILL BE DONE / SET TO YES TO ACTUALLY MAKE THE CHANGE

    -------------------------------------------------------------------------------------------------------------------------------------------------------------------

    DECLARE @bar_code_id INT = 0, 
            @bar_code_data VARCHAR(50) = '', 
            @ticket_no INT = 0, 
            @sli_no INT = 0, 
            @ord_no INT = 0, 
            @customer_no INT = 0,
            @fiscal_year INT = 0, 
            @perf_date CHAR(10) = '', 
            @perf_time VARCHAR(8) = '', 
            @customer_name VARCHAR(100) = ''

    DECLARE @new_perf_date CHAR(10) = '', 
            @new_ticket_no INT = 0, 
            @new_ticket_fiscal_year INT = 0

    DECLARE @new_sli_no INT = 0, 
            @new_ord_no INT = 0

    DECLARE @eMsg VARCHAR(1000) = ''

    DECLARE @results TABLE ([new_ticket_fiscal_year] INT, 
                            [bar_code_id] INT, 
                            [bar_code_data] VARCHAR(50), 
                            [customer_no] INT, 
                            [customer_name] VARCHAR(100), 
                            [prev_ord_no] INT, 
                            [prev_sli_no] INT, 
                            [prev_perf_date] CHAR(10), 
                            [prev_ticket_no] INT, 
                            [new_ord_no] int, 
                            [new_sli_no] INT, 
                            [new_perf_date] CHAR(10), 
                            [new_ticket_no] INT)


BEGIN TRANSACTION

    DECLARE bar_code_cursor INSENSITIVE CURSOR FOR 
    SELECT bar.[id],
           bar.[barcode_data],
           bar.[ticket_no],
           tkt.[sli_no], 
           sli.[order_no],
           ord.[customer_no],
           prf.[season_fiscal_year],
           prf.[performance_date],
           prf.[performance_time]
    FROM [dbo].[T_EXTERNAL_BARCODE_DATA] AS bar
           LEFT OUTER JOIN [dbo].[TX_SLI_TICKET] AS tkt ON tkt.[ticket_no] = bar.[ticket_no]
           LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[sli_no] = tkt.[sli_no]
           LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
           LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
    WHERE prf.season_fiscal_year = @old_fiscal_year
    OPEN bar_code_cursor
    BEGIN_BAR_CODE_LOOP:

        BEGIN TRY

            FETCH NEXT FROM bar_code_cursor INTO @bar_code_id, @bar_code_data, @ticket_no, @sli_no, @ord_no, @customer_no, 
                                                 @fiscal_year, @perf_date, @perf_time
            IF @@FETCH_STATUS = -1 GOTO END_BAR_CODE_LOOP

            SELECT @new_perf_date = CONVERT(CHAR(4),@old_fiscal_year) + '/07/01';

            WITH CTE_PERFS ([performance_no], [performance_zone], [performance_date], [performance_time], [season_fiscal_year])
            AS (SELECT [performance_no], [performance_zone], [performance_date], [performance_time], [season_fiscal_year] 
                FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_date] = @new_perf_date)
            SELECT @new_sli_no = sli.[sli_no], 
                   @new_ord_no = ord.[order_no], 
                   @new_ticket_no = sli.[ticket_no], 
                   @new_ticket_fiscal_year = prf.[season_fiscal_year]
            FROM [dbo].[T_SUB_LINEITEM] AS sli
                 LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
                 LEFT OUTER JOIN [CTE_PERFS] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
            WHERE ord.[customer_no] = @customer_no AND prf.[performance_date] = @new_perf_date AND prf.[performance_time] = @perf_time AND sli.[sli_status] IN (3,12)
            
            IF @new_ticket_fiscal_year <= @fiscal_year GOTO BEGIN_BAR_CODE_LOOP

            IF ISNULL(@new_ticket_no,0) = 0 GOTO BEGIN_BAR_CODE_LOOP
            
            SELECT @customer_name = ISNULL([display_name],'') FROM dbo.FT_CONSTITUENT_DISPLAY_NAME() WHERE [customer_no] = @customer_no
            
            IF @change_data = 'Yes' BEGIN 
                                        UPDATE [dbo].[T_EXTERNAL_BARCODE_DATA] 
                                        SET [ticket_no] = @new_ticket_no 
                                        WHERE [id] = @bar_code_id

                                        --PRINT @@ROWCOUNT
                                    END
            
            INSERT INTO @results
            VALUES (@new_ticket_fiscal_year, @bar_code_id, @bar_code_data, @customer_no, @customer_name, @ord_no, @sli_no, 
                    @perf_date, @ticket_no, @new_ord_no, @new_sli_no, @new_perf_date, @new_ticket_no)
 
        END TRY
          BEGIN CATCH

              WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION

              SELECT 'An error has occurred.' + CHAR(10) + ISNULL(ERROR_MESSAGE(),'') + CHAR(10) + 'Transaction Rolled Back.';

              THROW 51000, @emsg, 1;

              GOTO END_BAR_CODE_LOOP

          END CATCH
   
    GOTO BEGIN_BAR_CODE_LOOP

END_BAR_CODE_LOOP:
CLOSE bar_code_cursor
DEALLOCATE bar_code_cursor


    WHILE @@TRANCOUNT > 0 COMMIT TRANSACTION

DONE:

    WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION

SELECT * FROM @results




--SELECT * FROM dbo.T_EXTERNAL_BARCODE_DATA