/*  This script will produce three different lists.
    The first is all active reports, the second is all active utilities, 
    and the third is inactive reports and utilities.  */

PRINT ''
SELECT CONVERT(VARCHAR(30),'Active Reports') AS 'selection', 
       CONVERT(VARCHAR(50),GETDATE()) AS 'data_accurate_as_of'

SELECT ct.[description] AS 'report_folder', 
       rp.[name] AS 'report_name', 
       tp.[Description] AS 'report_type', 
       rp.[utility_ind] AS 'is_utility', 
       rp.inactive AS 'is_inactive', 
       ISNULL(REPLACE(REPLACE(rp.[description],CHAR(13),''),CHAR(10),''),'') AS 'report_description'
FROM dbo.gooesoft_report AS rp (NOLOCK)
     INNER JOIN dbo.gooesoft_report_category AS ct (NOLOCK) ON ct.id = rp.category
     INNER JOIN dbo.TR_REPORT_TYPE AS tp (NOLOCK) ON tp.id = rp.report_type
WHERE rp.[inactive] = 'N' AND rp.[utility_ind] = 'N'
ORDER BY ct.[Description], 
         rp.[name]

PRINT ''
SELECT CONVERT(VARCHAR(30),'Active Utilities') AS 'selection', 
       CONVERT(VARCHAR(50),GETDATE()) AS 'data_accurate_as_of'

SELECT ct.[description] AS 'report_folder', 
       rp.[name] AS 'report_name', 
       tp.[Description] AS 'report_type', 
       rp.[utility_ind] AS 'is_utility', 
       rp.inactive AS 'is_inactive', 
       ISNULL(REPLACE(REPLACE(rp.[description],CHAR(13),''),CHAR(10),''),'') AS 'report_description'
FROM dbo.gooesoft_report AS rp (NOLOCK)
     INNER JOIN dbo.gooesoft_report_category AS ct (NOLOCK) ON ct.id = rp.category
     INNER JOIN dbo.TR_REPORT_TYPE AS tp (NOLOCK) ON tp.id = rp.report_type
WHERE rp.[inactive] = 'N' AND rp.[utility_ind] = 'Y'
ORDER BY ct.[Description], 
         rp.[name]

PRINT ''
SELECT CONVERT(VARCHAR(30),'Inactive Reports & Utilities') AS 'selection', 
       CONVERT(VARCHAR(50),GETDATE()) AS 'data_accurate_as_of'

SELECT ct.[description] AS 'report_folder', 
       rp.[name] AS 'report_name', 
       tp.[Description] AS 'report_type', 
       rp.[utility_ind] AS 'is_utility', 
       rp.inactive AS 'is_inactive', 
       ISNULL(REPLACE(REPLACE(rp.[description],CHAR(13),''),CHAR(10),''),'') AS 'report_description'
FROM dbo.gooesoft_report AS rp (NOLOCK)
     INNER JOIN dbo.gooesoft_report_category AS ct (NOLOCK) ON ct.id = rp.category
     INNER JOIN dbo.TR_REPORT_TYPE AS tp (NOLOCK) ON tp.id = rp.report_type
WHERE rp.[inactive] = 'Y'
ORDER BY ct.[Description], 
         rp.[utility_ind], 
         rp.[name]


