-- GET ACTIVE MEMBERSHIPS AS OF A DATE
-- NEED TO LOOK IN BOTH VIEWS - LV_CURRENT_MEMBERSHIP_INFO AND LV_PAST_MEMBERSHIP_INFO

DECLARE @MembershipActiveDate DATETIME = '2016/06/30'

DECLARE @ActiveMembershipsTbl TABLE
(
	 customer_no INT,
	 customer_display_name VARCHAR(200),
	 memb_level_category_description VARCHAR(30), 
	 memb_level_code VARCHAR(3), 
	 initiation_date DATETIME, 
	 expiration_date DATETIME, 
	 current_status_desc VARCHAR(30) 
)

INSERT INTO @ActiveMembershipsTbl
(
	customer_no,
	customer_display_name,
	memb_level_category_description,
	memb_level_code,
	initiation_date,
	expiration_date,
	current_status_desc
)
SELECT customer_no, customer_display_name,memb_level_category_description, memb_level_code, initiation_date, expiration_date, 'Active' AS current_status_desc
FROM dbo.LV_CURRENT_MEMBERSHIP_INFO
WHERE @MembershipActiveDate BETWEEN inception_date AND expiration_date
UNION ALL 
-- Don't want to double count any non-current memberships if they are already ACTIVE.
SELECT customer_no, customer_display_name,memb_level_category_description, memb_level_code, initiation_date, expiration_date, current_status_desc
FROM dbo.LV_PAST_MEMBERSHIP_INFO
WHERE @MembershipActiveDate BETWEEN inception_date AND expiration_date
AND customer_no NOT IN (SELECT customer_no FROM dbo.LV_CURRENT_MEMBERSHIP_INFO WHERE @MembershipActiveDate BETWEEN inception_date AND expiration_date) 

-- Get counts of all Active MOS Members by category and level
SELECT memb_level_category_description, memb_level_code, COUNT(*) AS cnt
FROM @ActiveMembershipsTbl
GROUP BY memb_level_category_description, memb_level_code
ORDER BY memb_level_category_description, memb_level_code

-- Get list of all Active MOS members with membership category and level information 
SELECT * 
FROM @ActiveMembershipsTbl
--WHERE customer_display_name LIKE '%Manooshian%' OR customer_display_name LIKE '%Ippolito%'
ORDER BY memb_level_category_description, memb_level_code, customer_display_name

-- SANITY CHECK 
-- CHECK FOR DUPLICATE CUSTOMER_no
	SELECT customer_no, COUNT(*) cnt
	FROM @ActiveMembershipsTbl
	GROUP BY customer_no
	HAVING COUNT(*) > 1