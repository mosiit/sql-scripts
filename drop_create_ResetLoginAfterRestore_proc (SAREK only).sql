USE [admin]
GO

/****** Object:  StoredProcedure [dbo].[ResetLoginAfterRestore]    Script Date: 11/15/2017 11:54:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE [dbo].[ResetLoginAfterRestore]
GO

CREATE PROCEDURE [dbo].[ResetLoginAfterRestore]
	@RunNumber BIGINT
AS

BEGIN

DECLARE	@cnt INT,
		@max INT,
		@name VARCHAR(100),
		@sid VARBINARY(85),
		@ncmd_create NVARCHAR(MAX),
		@ncmd_alter NVARCHAR(MAX),
		@ncmd_view NVARCHAR(MAX),
		@ncmd_showp NVARCHAR(MAX)

DECLARE	@tblLogins TABLE	(
							RowID	INT IDENTITY(1,1),
							SecID	VARBINARY(85)
							)

SET NOCOUNT ON

INSERT INTO @tblLogins
	SELECT	[SID]
	FROM	sys.syslogins
	WHERE   dbname = 'impresario'
	AND		hasaccess = 1
	AND		isntname = 1
	AND		sysadmin <> 1
	--and		[name] = 'MOS.ORG\showe'
	
SELECT	@max = COUNT(RowID) 
FROM	@tblLogins

SELECT	@cnt = 1

WHILE	@cnt <= @max
	BEGIN
		
		SELECT	@sid = SecId
		FROM	@tblLogins
		WHERE	RowID = @cnt

		SELECT	@name = [name]
		FROM	sys.syslogins
		WHERE	[sid] = @sid
		
		SELECT	@ncmd_create = 'USE [impresario] CREATE USER [' + @name + '] FOR LOGIN [' + @name + '] ALTER ROLE [db_datareader] ADD MEMBER [' + @name + '] ALTER ROLE [ImpUsers] ADD MEMBER [' + @name + '] USE [master] ALTER LOGIN [' + @name + '] WITH DEFAULT_DATABASE=[impresario], DEFAULT_LANGUAGE=[us_english]'
		--SELECT	@ncmd_create

		SELECT	@ncmd_alter = 'USE [impresario] ALTER ROLE [db_datareader] ADD MEMBER [' + @name + '] ALTER ROLE [ImpUsers] ADD MEMBER [' + @name + '] USE [master] ALTER LOGIN [' + @name + '] WITH DEFAULT_DATABASE=[impresario], DEFAULT_LANGUAGE=[us_english]'
		--SELECT	@ncmd_alter

		-- H. Sheridan, 20171115 - Add ability to show plans to list of users grabbed above
		SELECT	@ncmd_showp = 'USE [impresario] GRANT SHOWPLAN TO [' + @name + ']'
		--SELECT	@ncmd_showp

		BEGIN TRY

			IF NOT EXISTS (SELECT [Login] FROM [admin].[dbo].[ResetLoginStatusLog] WHERE [RunNumber] = @RunNumber AND [Login] = @name)

				BEGIN TRY

					EXEC sp_executesql @ncmd_create

					INSERT INTO [admin].[dbo].[ResetLoginStatusLog]
						SELECT @RunNumber, @name, 'Granted read-only and ImpUsers access, default database set to impresario'

					EXEC sp_executesql @ncmd_showp

					INSERT INTO [admin].[dbo].[ResetLoginStatusLog]
						SELECT @RunNumber, @name, 'Granted show plan privileges'
				
				END TRY

				BEGIN CATCH

					EXEC sp_executesql @ncmd_alter

					INSERT INTO [admin].[dbo].[ResetLoginStatusLog]
						SELECT @RunNumber, @name, 'Granted read-only and ImpUsers access, default database set to impresario'
	
				END CATCH


		END TRY

		BEGIN CATCH
			IF NOT EXISTS (SELECT [Login] FROM [admin].[dbo].[ResetLoginStatusLog] WHERE [RunNumber] = @RunNumber AND [Login] = @name)
				INSERT INTO [admin].[dbo].[ResetLoginStatusLog]
					SELECT @RunNumber, @name, 'Access okay - no changes needed'

		END CATCH

		SELECT	@cnt = @cnt + 1
	END

	-- H. Sheridan, 12/6/2016 - Regardless of success or failure, make sure everyone in ImpUsers account can view object definitions in the dbo schema
	SELECT	@ncmd_view = 'USE [impresario] GRANT VIEW DEFINITION ON SCHEMA :: dbo TO ImpUsers'
	EXEC sp_executesql @ncmd_view

END	



GO


