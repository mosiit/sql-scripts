DECLARE @user_table table ([user_id] varchar(10), [user_name] varchar(50), [user_location] varchar(30))
DECLARE @report_dt datetime             SELECT @report_dt = '6-26-2018'
DECLARE @operator_loc varchar(30)       SELECT @operator_loc = 'Box Office'

    INSERT INTO @user_table
    SELECT [userid], ([fname] + ' ' + [lname]), [location] FROM [dbo].[T_METUSER]
    WHERE [userid] in (SELECT [created_by] FROM [dbo].[T_SUB_LINEITEM] WHERE convert(char(10),[create_dt],111) = convert(char(10),@report_dt,111))
    ORDER BY [lname], [fname]

    IF @operator_loc <> '' DELETE FROM @user_table WHERE [user_location] <> @operator_loc

    SELECT * FROM @user_table


