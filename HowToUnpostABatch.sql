-- To unpost a batch:
-- 1. Update the batch number below 
-- 2. Copy the results of the query and run it. 

SELECT  [batch_no],
        [post_no],
       'EXEC UP_UNPOST @post_no = ' + CONVERT(VARCHAR(10), [post_no])
FROM    [impresario].[dbo].[T_BATCH]
WHERE  [batch_no] IN (12670)


-- Example of the query returned by above select statement
--EXEC UP_UNPOST @post_no = 5120