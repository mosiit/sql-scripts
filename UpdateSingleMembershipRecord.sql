-- **********************************************************
-- Use this SELECT to find the affected record.  You may want
-- to filter by customer_no only to start (assuming you don't
-- have the cust_memb_no).  If you have both, great!
-- **********************************************************
SELECT  current_status,
        cur_record,
        *
FROM    dbo.TX_CUST_MEMBERSHIP
WHERE   customer_no = 829968
        AND cust_memb_no = 1084017

-- **********************************************************
-- Use this SELECT to find the status values.
-- **********************************************************
SELECT  *
FROM    dbo.TR_CURRENT_STATUS
-- 1 = Inactive (current value)
-- 2 = Active (change to value)

-- **********************************************************
-- When you are ready to update the record, comment out this
-- statement, replacing all values as needed.
-- **********************************************************
--UPDATE  dbo.TX_CUST_MEMBERSHIP
--SET     current_status = 2,
--        cur_record = 'Y',
--		expr_dt = '2017-11-30 00:00:00.000'
--WHERE   customer_no = 829968
--        AND cust_memb_no = 1084017
---- 1 row affected
