SELECT	rep.[name] AS 'Report Name',
		--ISNULL(rep.[description], '') AS 'Report description',
		rc.[description] AS 'Category',
		ISNULL(rep.[psr_file], '') AS 'PSR File',
		rt.[description] AS 'Report Type',
		req.id AS 'Report ID',
		req.[user_id] AS 'User ID',
		req.[ug_id] AS 'User Group',
		req.request_date_time AS 'Start Date',
		req.end_date_time As 'End Date',
		ISNULL(req.[result_code], '') AS 'Result Code',
		ISNULL(req.[printer_name], '') AS 'Printer Name'
		--ISNULL(req.[email_recipients], '') AS 'Email Recipients'
FROM	gooesoft_request req
JOIN	gooesoft_report rep
ON		req.[report_id] = rep.id
JOIN	[TR_REPORT_TYPE] rt
ON		rt.[id] = rep.[report_type]		
JOIN	[gooesoft_report_category] rc
ON		rc.[id] = rep.[category]
WHERE	DATEDIFF(dd, req.request_date_time, GetDate()) = 0 
ORDER BY req.request_date_time